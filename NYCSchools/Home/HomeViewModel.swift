import Foundation

class HomeViewModel {
    static var maxHomeListCount: Int = 5
    
    private let apiService: SchoolAPIServiceProtocol

    var state: APIServiceState = .idle
    var schools: [School] = []

    init(apiService: SchoolAPIServiceProtocol = SchoolAPIService()) {
        self.apiService = apiService
    }

    func fetch() async {
        state = .loading
        do {
            schools = try await apiService.fetchSchools(
                limit: HomeViewModel.maxHomeListCount,
                offset: 0
            )
            state = schools.isEmpty ? .empty : .ready
        } catch {
            state = .error(error as? SchoolAPIServiceError)
        }
    }
}
