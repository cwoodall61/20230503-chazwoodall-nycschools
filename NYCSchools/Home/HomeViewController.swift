import UIKit

class HomeViewController: UITableViewController {
    static let height: CGFloat = 50
    static let cellIdentifier = "HomeTableViewCellIdentifier"
    static let emptyTableMessage = "No Schools Available."

    private let viewModel: HomeViewModel
    private var indicator: UIViewController?

    init(viewModel: HomeViewModel = HomeViewModel()) {
        self.viewModel = viewModel
        super.init(style: .insetGrouped)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "NYC Schools"
        navigationController?.navigationBar.prefersLargeTitles = true
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: Self.cellIdentifier)

        let accessorySize = CGRect(
            x: .zero,
            y: .zero,
            width: UIScreen.main.bounds.width,
            height: Self.height
        )
        let headerView = HomeTableHeaderView(frame: accessorySize)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showBookmarkView))
        headerView.addGestureRecognizer(tapGesture)
        tableView.tableHeaderView = headerView
        fetchData()
    }

    private func fetchData() {
        Task {
            await viewModel.fetch()

            // This could be substituted and replaced with some combine publishers.
            switch viewModel.state {
            case .idle, .loading:
                indicator = showLoadingIndicator()
            case .empty:
                showEmptyTable(with: Self.emptyTableMessage)
            case .error:
                showEmptyTable(with: Self.emptyTableMessage)
            case .ready:
                tableView.reloadData()
            }
            removeLoadingIndicator(indicator)
        }
    }

    private func showEmptyTable(with message: String) {
        let label = UILabel()
        label.text = message
        view.addSubview(label)
        label.center = view.center
    }

    @objc private func showBookmarkView() {
        let viewController = BookmarkListViewController(style: .insetGrouped)
        viewController.modalPresentationStyle = .pageSheet
        self.present(UINavigationController(rootViewController: viewController), animated: true)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.schools.count + 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: Self.cellIdentifier)
        cell.selectionStyle = .none
        // If this is the last row in the tableView change the configuration.
        guard indexPath.row < viewModel.schools.count else {
            cell.textLabel?.text = "See All"
            cell.accessoryType = .disclosureIndicator
            return cell
        }

        cell.textLabel?.text = viewModel.schools[indexPath.row].name
        cell.detailTextLabel?.text = viewModel.schools[indexPath.row].overview
        cell.accessoryType = .detailButton
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var viewController: UIViewController

        if indexPath.row + 1 > viewModel.schools.count {
            viewController = SchoolListViewController()
        } else {
            let school = viewModel.schools[indexPath.row]
            viewController = SchoolDetailViewController(viewModel: SchoolDetailViewModel(school: school))
        }

        viewController.modalPresentationStyle = .pageSheet
        self.present(UINavigationController(rootViewController: viewController), animated: true)
    }
}
