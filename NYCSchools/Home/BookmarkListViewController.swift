//
//  BookmarkListViewController.swift
//  NYCSchools
//
//  Created by Chaz Woodall on 5/2/23.
//

import UIKit

class BookmarkListViewController: UITableViewController {
    static let cellIdentifier = "BookmarkTableViewCellIdentifier"

    private var bookmarks: [School] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Bookmarks"
        navigationController?.navigationBar.prefersLargeTitles = true
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: Self.cellIdentifier)

        fetchData()
    }

    private func fetchData() {
        bookmarks = (try? SchoolBookmarkManager.fetchBookmarks()) ?? []
        tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookmarks.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: Self.cellIdentifier)
        cell.selectionStyle = .none
        cell.textLabel?.text = bookmarks[indexPath.row].name
        cell.detailTextLabel?.text = bookmarks[indexPath.row].overview
        cell.accessoryType = .detailButton
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let school = bookmarks[indexPath.row]
        let viewController = SchoolDetailViewController(viewModel: SchoolDetailViewModel(school: school))
        viewController.modalPresentationStyle = .pageSheet
        self.present(UINavigationController(rootViewController: viewController), animated: true)
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            do {
                try SchoolBookmarkManager.unbookmark(school: bookmarks[indexPath.row])
                bookmarks.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
            } catch {
                showErrorAlert(errorString: "There was an error unbookmarking this school")
            }
        }
    }

}
