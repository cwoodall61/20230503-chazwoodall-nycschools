import UIKit

class HomeTableHeaderView: UIView {
    static let padding: CGFloat = 10

    let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Bookmarks"
        label.textColor = .systemBlue
        return label
    }()

    let image: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(systemName: "chevron.right")
        return imageView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(label)
        addSubview(image)
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Self.padding),
            label.centerYAnchor.constraint(equalTo: centerYAnchor),

            image.leadingAnchor.constraint(equalTo: label.trailingAnchor, constant: Self.padding),
            image.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
