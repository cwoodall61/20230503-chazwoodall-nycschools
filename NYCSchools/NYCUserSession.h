//
//  NYCUserSession.h
//  NYCSchools
//
//  Created by Chaz Woodall on 5/2/23.
//

#import <Foundation/Foundation.h>
#import "NYCSchools-Swift.h"

NS_SWIFT_NAME(UserSession)
@interface NYCUserSession : NSObject

- (void)simulateIntenseLoginTask:(NYCUser *)user;

@end
