import UIKit

class SchoolListViewController: UITableViewController {
    static let height: CGFloat = 50
    static let cellIdentifier = "SchoolListTableViewCellIdentifier"

    private let viewModel: SchoolListViewModel
    private var indicator: UIViewController?

    init(viewModel: SchoolListViewModel = SchoolListViewModel()) {
        self.viewModel = viewModel
        super.init(style: .insetGrouped)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "All Schools"
        navigationController?.navigationBar.prefersLargeTitles = true
        tableView.register(
            UITableViewCell.self,
            forCellReuseIdentifier: Self.cellIdentifier
        )

        let accessorySize = CGRect(
            x: .zero,
            y: .zero,
            width: UIScreen.main.bounds.width,
            height: Self.height
        )
        let footerView = SchoolListTableFooterView(frame: accessorySize)
        footerView.button.addTarget(self, action: #selector(fetchData), for: .touchUpInside)
        tableView.tableFooterView = footerView

        fetchData()
    }

    @objc
    private func fetchData() {
        Task {
            await viewModel.fetch()

            // This could be substituted and replaced with some combine publishers.
            switch viewModel.state {
            case .idle, .loading:
                indicator = showLoadingIndicator()
            case .empty:
                showEmptyTable(with: "")
            case .error:
                showEmptyTable(with: "")
            case .ready:
                tableView.reloadData()
                tableView.scrollToRow(
                    at: IndexPath(row: .zero, section: .zero),
                    at: .top,
                    animated: true
                )
            }
            removeLoadingIndicator(indicator)
        }
    }

    private func showEmptyTable(with message: String) {}

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.schools.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: Self.cellIdentifier)
        cell.selectionStyle = .none
        cell.textLabel?.text = viewModel.schools[indexPath.row].name
        cell.detailTextLabel?.text = viewModel.schools[indexPath.row].overview
        cell.accessoryType = .detailButton
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let school = viewModel.schools[indexPath.row]
        let viewController = SchoolDetailViewController(
            viewModel: SchoolDetailViewModel(school: school)
        )
        viewController.modalPresentationStyle = .pageSheet
        self.present(
            UINavigationController(rootViewController: viewController),
            animated: true
        )
    }
}
