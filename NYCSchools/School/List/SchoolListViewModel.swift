//
//  SchoolListViewModel.swift
//  NYCSchools
//
//  Created by Chaz Woodall on 5/2/23.
//

import Foundation

class SchoolListViewModel {
    static let batchCount = 100

    private let apiService: SchoolAPIServiceProtocol
    
    var state: APIServiceState = .idle
    var currentListOffset = 0
    var schools: [School] = []

    init(apiService: SchoolAPIServiceProtocol = SchoolAPIService()) {
        self.apiService = apiService
    }
    
    func fetch() async {
        state = .loading
        do {
            schools = try await apiService.fetchSchools(
                limit: Self.batchCount,
                offset: currentListOffset
            )
            currentListOffset += Self.batchCount
            state = schools.isEmpty ? .empty : .ready
        } catch {
            state = .error(error as? SchoolAPIServiceError)
        }
    }
}
