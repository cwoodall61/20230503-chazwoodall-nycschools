import Foundation

enum SchoolBookmarkManagerError: Error {
    case didNotFindSchoolToRemove
}

class SchoolBookmarkManager {
    static let schoolsKey = "schools"

    static func fetchBookmarks() throws -> [School] {
        guard let data = UserDefaults.standard.data(forKey: Self.schoolsKey) else {
            return []
        }

        let decoder = JSONDecoder()
        let schools = try decoder.decode([School].self, from: data)
        return schools
    }

    @discardableResult
    static func fetchBookmark(school: School) throws -> School? {
        let schools = try Self.fetchBookmarks()
        return schools.first { $0 == school }
    }

    static func bookmark(school: School) throws {
        var schools = try Self.fetchBookmarks()
        schools.append(school)

        let encoder = JSONEncoder()
        let encodedSchools = try encoder.encode(schools)
        UserDefaults.standard.set(encodedSchools, forKey: Self.schoolsKey)
    }

    static func unbookmark(school: School) throws {
        var schools = try Self.fetchBookmarks()
        guard let index = schools.firstIndex(where: { $0 == school }) else {
            throw SchoolBookmarkManagerError.didNotFindSchoolToRemove
        }

        schools.remove(at: index)
        let encoder = JSONEncoder()
        let encodedSchools = try encoder.encode(schools)
        UserDefaults.standard.set(encodedSchools, forKey: Self.schoolsKey)
    }
}
