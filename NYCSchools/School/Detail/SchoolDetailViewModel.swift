//
//  SchoolDetailViewModel.swift
//  NYCSchools
//
//  Created by Chaz Woodall on 5/2/23.
//

import Foundation

class SchoolDetailViewModel {

    private let apiService: SchoolAPIServiceProtocol

    let school: School

    var state: APIServiceState = .idle
    var satResults: [String: String] = [:]

    init(school: School, apiService: SchoolAPIServiceProtocol = SchoolAPIService()) {
        self.school = school
        self.apiService = apiService
    }

    func fetch() async {
        state = .loading
        do {
            satResults = try await apiService.fetchSATResults(for: school)
            state = satResults.isEmpty ? .empty : .ready
        } catch {
            state = .error(error as? SchoolAPIServiceError)
        }
    }
}
