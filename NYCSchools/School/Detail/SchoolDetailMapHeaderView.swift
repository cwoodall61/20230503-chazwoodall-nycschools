//
//  SchoolDetailMapHeaderView.swift
//  NYCSchools
//
//  Created by Chaz Woodall on 5/2/23.
//

import SwiftUI
import MapKit

struct SchoolDetailMapHeaderView: View {
    struct SchoolAnnotation: Identifiable {
        var id: UUID = UUID()
        let location: CLLocationCoordinate2D
    }

    @State private var region: MKCoordinateRegion = MKCoordinateRegion(
        center: CLLocationCoordinate2D(latitude: .zero, longitude: .zero),
        span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
    )
    @State var schoolAnnotations: [Self.SchoolAnnotation] = []

    var school: School

    var body: some View {
        Group {
            Map(coordinateRegion: $region, annotationItems: schoolAnnotations) {
                MapMarker(coordinate: $0.location)
            }
        }
        .onAppear {
            guard let lat = Double(school.latitude),
                  let long = Double(school.longitude)
            else { return }
            region = MKCoordinateRegion(
                center: CLLocationCoordinate2D(latitude: lat, longitude: long),
                span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
            )
            schoolAnnotations.append(
                SchoolAnnotation(
                    location: CLLocationCoordinate2D(latitude: lat, longitude: long)
                )
            )
        }
    }
}
