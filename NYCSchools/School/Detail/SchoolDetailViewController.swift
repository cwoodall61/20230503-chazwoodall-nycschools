import UIKit
import SwiftUI

class SchoolDetailViewController: UITableViewController {
    static let cellIdentifier = "SchoolDetailTableViewCellIdentifier"

    private var indicator: UIViewController?
    private let viewModel: SchoolDetailViewModel

    init(viewModel: SchoolDetailViewModel) {
        self.viewModel = viewModel
        super.init(style: .insetGrouped)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = viewModel.school.name

        // SwiftUI interop currently for using views in view controllers.
        let mapHeaderViewController = UIHostingController(
            rootView: SchoolDetailMapHeaderView(school: viewModel.school)
        )
        addChild(mapHeaderViewController)
        mapHeaderViewController.view.frame = CGRect(
            x: 0,
            y: 0,
            width: UIScreen.main.bounds.width - 40,
            height: 250
        )
        view.addSubview(mapHeaderViewController.view)
        mapHeaderViewController.didMove(toParent: self)
        tableView.tableHeaderView = mapHeaderViewController.view

        if viewModel.school.isBookmarked {
            navigationItem.rightBarButtonItem = UIBarButtonItem(
                image: UIImage(systemName: "checkmark"),
                style: .plain,
                target: self,
                action: #selector(unbookmark)
            )
        } else {
            navigationItem.rightBarButtonItem = UIBarButtonItem(
                barButtonSystemItem: .add,
                target: self,
                action: #selector(bookmark)
            )
        }

        fetchData()
    }

    private func fetchData() {
        Task {
            await viewModel.fetch()

            // This could be substituted and replaced with some combine publishers.
            switch viewModel.state {
            case .idle, .loading:
                indicator = showLoadingIndicator()
            case .empty:
                showEmptyTable(with: "No SAT Results found for this school")
            case .error:
                showEmptyTable(with: "No SAT Results found for this school")
            case .ready:
                tableView.reloadData()
                tableView.scrollToRow(
                    at: IndexPath(row: .zero, section: .zero),
                    at: .top,
                    animated: true
                )
            }
            removeLoadingIndicator(indicator)
        }
    }

    @objc func bookmark() {
        do {
            try SchoolBookmarkManager.bookmark(school: viewModel.school)
            navigationItem.rightBarButtonItem = UIBarButtonItem(
                image: UIImage(systemName: "checkmark"),
                style: .plain,
                target: self,
                action: #selector(unbookmark)
            )
        } catch {
            showErrorAlert(errorString: "There was an error saving this school")
        }
    }

    @objc func unbookmark() {
        do {
            try SchoolBookmarkManager.unbookmark(school: viewModel.school)
            navigationItem.rightBarButtonItem = UIBarButtonItem(
                barButtonSystemItem: .add,
                target: self,
                action: #selector(bookmark)
            )
        } catch {
            showErrorAlert(errorString: "There was an error unsaving this school")
        }
    }

    private func showEmptyTable(with message: String) {
        viewModel.satResults[message] = ""
        tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "SAT Results"
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.satResults.keys.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: Self.cellIdentifier)
        cell.selectionStyle = .none

        let keys = Array(viewModel.satResults.keys)
        let values = Array(viewModel.satResults.values)
        cell.textLabel?.text = keys[indexPath.row]
        cell.detailTextLabel?.text = values[indexPath.row]
        return cell
    }
}
