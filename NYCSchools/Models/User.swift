//
//  User.swift
//  NYCSchools
//
//  Created by Chaz Woodall on 5/2/23.
//

import Foundation

@objc(NYCUser)
class User: NSObject {
    var name: String

    init(name: String) {
        self.name = name
    }
}
