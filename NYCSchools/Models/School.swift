//
//  File.swift
//  NYCSchools
//
//  Created by Chaz Woodall on 5/2/23.
//

import Foundation

struct School: Codable, Equatable {
    var dbn: String
    var name: String
    var boro: String
    var overview: String
    var location: String
    var phone: String
    var website: String
    var latitude: String
    var longitude: String
    
    // The school email is not available for all schools.
    var email: String?

    var isBookmarked: Bool {
        return (try? SchoolBookmarkManager.fetchBookmark(school: self)) != nil
    }

    enum CodingKeys: String, CodingKey {
        case dbn
        case name = "school_name"
        case boro
        case overview = "overview_paragraph"
        case location
        case phone = "phone_number"
        case email = "school_email"
        case website
        case latitude
        case longitude
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.dbn = try container.decode(String.self, forKey: .dbn)
        self.name = try container.decode(String.self, forKey: .name)
        self.boro = try container.decode(String.self, forKey: .boro)
        self.overview = try container.decode(String.self, forKey: .overview)
        self.location = try container.decode(String.self, forKey: .location)
        self.phone = try container.decode(String.self, forKey: .phone)
        self.website = try container.decode(String.self, forKey: .website)
        self.latitude = try container.decode(String.self, forKey: .latitude)
        self.longitude = try container.decode(String.self, forKey: .longitude)

        self.email = try? container.decode(String.self, forKey: .email)
    }
}
