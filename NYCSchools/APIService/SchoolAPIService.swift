//
//  APIService.swift
//  NYCSchools
//
//  Created by Chaz Woodall on 5/2/23.
//

import Foundation

enum APIServiceState {
    case idle
    case loading
    case ready
    case empty
    case error(Error?)
}

enum SchoolAPIServiceError: Error {
    case unableToUnwrapURL
    case unableToUnwrapData
    case unknownServerError
    case noDictionaryFound
}

// Protocol based API service in order to mock for Unit Tests.
protocol SchoolAPIServiceProtocol {
    func fetchSchools(limit: Int, offset: Int) async throws -> [School]
    // Instead of creating a model here, let's use a dictionary becuase it will be
    // much easier to render in our detail view. Using a struct would require either
    // alot of boiler plate and manual code or reflection.
    func fetchSATResults(for school: School) async throws -> [String: String]
}

struct SchoolAPIService: SchoolAPIServiceProtocol {
    static let schoolsURLPath = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let satURLPath = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"

    /// Makes a request to gather all schools within the City of New York database.
    func fetchSchools(limit: Int = 0, offset: Int = 0) async throws -> [School] {
        var components = URLComponents(string: Self.schoolsURLPath)
        components?.queryItems = [
            URLQueryItem(name: "$limit", value: String(limit)),
            URLQueryItem(name: "$offset", value: String(offset))
        ]

        guard let url = components?.url else {
            throw SchoolAPIServiceError.unableToUnwrapURL
        }
        let request = URLRequest(url: url)
        let (data, _) = try await URLSession.shared.data(for: request)
        return try JSONDecoder().decode([School].self, from: data)
    }

    /// Makes a request to gather the SAT results for a school within the City of New York database.
    func fetchSATResults(for school: School) async throws -> [String: String] {
        var components = URLComponents(string: Self.satURLPath)
        components?.queryItems = [
            URLQueryItem(name: "dbn", value: school.dbn)
        ]

        guard let url = components?.url else {
            throw SchoolAPIServiceError.unableToUnwrapURL
        }
        let request = URLRequest(url: url)
        let (data, _) = try await URLSession.shared.data(for: request)
        let resultsArray = try JSONDecoder().decode([[String: String]].self, from: data)
        
        guard let result = resultsArray.first else {
            throw SchoolAPIServiceError.noDictionaryFound
        }
        return result
    }
}
