import UIKit

extension UIViewController {
    /// Show a loading indicator on the current view controller.
    /// Parameters -
    ///     timeout: A `TimeInterval` to remove the loading indicator
    ///     action: A closure to run an action after the `TimeInterval` is completed.
    func showLoadingIndicator(timeout: TimeInterval? = nil, action: (() -> Void)? = nil) -> UIViewController {
        let indicator = SpinnerViewController()
        addChild(indicator)
        indicator.view.frame = view.frame
        view.addSubview(indicator.view)
        indicator.didMove(toParent: self)

        guard let timeout else { return indicator }
        DispatchQueue.main.asyncAfter(deadline: .now() + timeout) { [weak self] in
            guard let self = self else { return }
            self.removeLoadingIndicator(indicator)
            action?()
        }
        return indicator
    }

    /// Remove the loading indicator on the current view controller.
    func removeLoadingIndicator(_ indicator: UIViewController?) {
        indicator?.willMove(toParent: nil)
        indicator?.view.removeFromSuperview()
        indicator?.removeFromParent()
    }

    /// Show an alert on the current view controller.
    func showErrorAlert(errorString: String) {
        let alertController = UIAlertController(title: errorString, message: nil, preferredStyle: .alert)
        let submitAction = UIAlertAction(title: "Done", style: .default)

        alertController.addAction(submitAction)
        present(alertController, animated: true)
    }
}
