//
//  LoginTask.m
//  NYCSchools
//
//  Created by Chaz Woodall on 5/2/23.
//

#import <Foundation/Foundation.h>
#import "NYCUserSession.h"
#import "NYCSchools-Swift.h"

@implementation NYCUserSession

- (instancetype)init
{
    self = [super init];
    if (self) {

    }
    return self;
}


- (void)simulateIntenseLoginTask:(NYCUser *)user {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                NSLog(@"Ran some intense task!");
            });
        }];
    });
}

@end
