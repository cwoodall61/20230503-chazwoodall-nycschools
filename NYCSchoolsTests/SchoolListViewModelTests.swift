//
//  NYCSchoolsTests.swift
//  NYCSchoolsTests
//
//  Created by Chaz Woodall on 5/2/23.
//

import XCTest
@testable import NYCSchools

struct MockSchoolAPIService: SchoolAPIServiceProtocol {
    func fetchSchools(limit: Int, offset: Int) async throws -> [School] {
        return []
    }

    func fetchSATResults(for school: NYCSchools.School) async throws -> [String : String] {
        return [:]
    }
}

final class SchoolListViewModelTests: XCTestCase {
    var viewModel: SchoolListViewModel!

    override func setUpWithError() throws {
        viewModel = SchoolListViewModel(apiService: MockSchoolAPIService())
    }

    func testBatchCalls() async throws {
        await viewModel.fetch()
        XCTAssertEqual(viewModel.currentListOffset, SchoolListViewModel.batchCount)

        await viewModel.fetch()
        XCTAssertEqual(viewModel.currentListOffset, SchoolListViewModel.batchCount * 2)

        await viewModel.fetch()
        XCTAssertEqual(viewModel.currentListOffset, SchoolListViewModel.batchCount * 3)
    }
}
